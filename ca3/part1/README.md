##VIRTUAL MACHINE - ASSINMENT

Part1
---

##Criar a máquina virtual

Dentro da Oracle VM Virtual Box criar uma nova máquina virtual de nome DevOps Ubuntu
Definir o sistema operativo Linux com Ubuntu 64-xp.
A quantidade de memória definida na máquina é de 2 GB e com um hard disk de 10 GB,
como pré-definido.
A virtual box é a definida pelo próprio sistema e o tipo de storage é o dynamicly allocated.

A máquina está assim criada e uma vez feito Start é possível liga-la como a um computador
normal...

##Definir a ligação à internet
Dentro de File temos Host Network Manager para o qual criaremos:

```sh
VirtualBox Host-Only Ethernet Adapter
IPv4 Address: 192.168.186.1
IPv4 Network Mask: 255.255.255.0
````

##
Temos a possibilidade de eligir até 4 adaptadores de rede, sendo que o primeiro
(Adaper 1) está já definido como NAT.
O Adapter 2 será então definido como Host-Only Adapter e ligado a 
VirtualBox Host-Only Ethernet Adapter.

##Ubuntu

```sh
Ubuntu 18.04 "Bionic Beaver"

````

##mini.iso
Descarregar o ficheiro do site do Ubuntu.
Nas Settings da VB definir em Storage a package que acabamos de descarregar (mini.iso).

Procede-se então à instalação do ubuntu, seguindo as indicações dadas pelo próprio
programa durante a instalação


##Instalações
Para poder fazer a instalação das ferramentas de network utilizar o seguinte comando:
sudo apt install net-tools


##part1
Fazer clone do repositorio Bitbucket dentro de devops
Para instalar o gradle com o curl é necessário instalar o curl
Instalar o git, jdk, maven, gradle

##instalar o gradle

```sh
$ curl -s "https://get.sdkman.io/" | bash

$ sudo apt install unzip

$ sudo apt install zip

source "/home/didi/.sdkman/bin/sdkman-init.sh"

$ sdk install gradle 6.3
````

##Failure do runClient
```sh
> Task :runClient FAILED
Exception in thread "main" java.awt.HeadlessException:
No X11 DISPLAY variable was set, but this program performed an operation which requires it.
        at java.awt.GraphicsEnvironment.checkHeadless(GraphicsEnvironment.java:204)
        at java.awt.Window.<init>(Window.java:536)
        at java.awt.Frame.<init>(Frame.java:420)
        at javax.swing.JFrame.<init>(JFrame.java:233)
        at basic_demo.ChatClient.<init>(ChatClient.java:35)
        at basic_demo.ChatClientApp.main(ChatClientApp.java:18)

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':runClient'.
> Process 'command '/usr/lib/jvm/java-8-openjdk-amd64/bin/java'' finished with non-zero exit value 1

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output. Run with --scan to get full insights.

* Get more help at https://help.gradle.org

Deprecated Gradle features were used in this build, making it incompatible with Gradle 7.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/6.3/userguide/command_line_interface.html#sec:command_line_warnings

BUILD FAILED in 9s

````

##Uma das soluções para correr cliente
Mudar o build.gradle 
```sh
task runClient
localhost = 192.168.56.5
````

##Solução sem mudar coisas
correr na cmd do host

```sh
 java -cp libs/basic_demo-0.1.0.jar basic_demo.ChatClientApp 192.168.56.5 59001

```
