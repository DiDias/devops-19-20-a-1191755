##Ca3 Assinment -Relatório


##Instalar o vagrant
Para proceder à instalção do vagrant foi feito o download através de:

````sh
https://www.vagrantup.com/downloads.html
````

A versão instalada foi a vagrant version 2.2.7

Para verificar qual a versão instalada, fazer o comando:
````sh
$vagrant -v
````



#Part2

Criar o diretório ca3-part2 e fazer clone da part2 da ca2 (versão mais atualizada
do projeto).
Colocar também dentro do diretório ca3-part2 um clone da Vagrantfile.

Após o clone, alterar o seguinte comando:
````sh
	git clone https://DiDias@bitbucket.org/DiDias/devops-19-20-a-1191755.git
      cd devops-19-20-a-1191755/ca3/part2/gradle-basic
      sudo rm -r node_modules
      sudo npm install
      sudo chmod +x gradlew
      sudo ./gradlew clean build

````
Também é necessário fazer a alteração no comando relativo ao tomcat, de 
````sh
$ sudo cp build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
````
Para:
````sh
$ sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
````
##Vagrant
Para correr o vagrant é necessário o comando:
````sh
vagrant up
````
São então criadas duas Virtual Machines dentro da Virtual Box.

No meu caso, em específico, tive de atualizar a Virtual Box para a versão mais recente,
pois não conseguia fazer a ligação ao Host da Ethernet.

##

Para ver o resultado, basta aceder ao browser com
 ````sh
http://localhost:8080/demo-0.0.1-SNAPSHOT/
````

ou 
````sh
http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/
````

Para pode aceder à consola H2 para realizar algum tipo de alteração à base de dados,
é possível através de:

````sh
http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
````

ou 

````sh
http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console
````

E, em qualquer um deles, modificar a connection string para:

````sh
jdbc:h2:tcp://192.168.33.11:9092/./jpadb
````

##Alternativa

A alternativa mais relevante à Virtual Box é o VMware.

Comparando as duas opções ambas são open source mas apenas para efeito pedagógico, ou uso
pessoal. Apresentam assim algumas limitações em todo o espólio de utilização, visto que a 
licença pro é vendida pelas suas respetivas companhias.

No caso do VMware a sua versão gratuita permite correr apenas uma Virtual Machine de
cada vez e não possui suporte oficial do vagrant.
Assim sendo, pode ser tomada em consideração a sua utilização num ambiente mais empresarial
e não para pequenos projetos de teste, como seria este caso.

Assim sendo, para este assinment a Virtual Box seria então a melhor opção.
