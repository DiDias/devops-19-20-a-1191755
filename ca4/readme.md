##DOCKER

Proceder a instalação do Docker Toolbox

````sh
https://docs.docker.com/toolbox/toolbox_install_windows/
````

O DOCKER instalado foi o 
````sh
Docker version 19.03.1 
````

Após a instalação, abrir  Docker QuickStart Terminal e irá abrir uma bash 
especial para Docker.

Para ver se o Docker está a funcionar corretamente correr o comando:
````sh
docker run hello-world   
````
e se tudo estiver a funcionar corretamente a seguinte mensagem irá aparecer:

````sh
$ docker run hello-world
 Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete                                                                                             Digest: sha256:8e3114318a995a1ee497790535e7b88365222a21771ae7e53687ad76563e8e76
Status: Downloaded newer image for hello-world:latest
 91c95931e552: Download complete
 a8219747be10: Download complete

 Hello from Docker!
 This message shows that your installation appears to be working correctly.

 To generate this message, Docker took the following steps:
  1. The Docker Engine CLI client contacted the Docker Engine daemon.
  2. The Docker Engine daemon pulled the "hello-world" image from the Docker Hub.
     (Assuming it was not already locally available.)
  3. The Docker Engine daemon created a new container from that image which runs the
     executable that produces the output you are currently reading.
  4. The Docker Engine daemon streamed that output to the Docker Engine CLI client, which sent it
     to your terminal.

 To try something more ambitious, you can run an Ubuntu container with:
  $ docker run -it ubuntu bash

 For more examples and ideas, visit:
  https://docs.docker.com/userguide/

````

Para correr o DOCKER no browser será o 

````sh
IP 192.168.99.101
````


##Ca4
Dentro do repositório local criar a pasta ca4.
Dentro desta devem ser clonadas as pastas:
* db,
* web,
* docker-compose-yml,
* data.

Estas deverão ser retiradas do repositório Bitbucket fornecido pelo professor.


##DOCKERFILE

Dentro do dockerfile de web mudar as linhas

```` sh
RUN git clone https://DiDias@bitbucket.org/DiDias/devops-19-20-a-1191755.git

WORKDIR /tmp/build/devops-19-20-a-1191755/ca3/part2/gradle-basic
RUN rm -r node_modules

RUN npm install
RUN chmod +x gradlew
RUN ./gradlew clean build

RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080

````


### Docker Hub
Criar um repositório Docker Hub

````sh
https://hub.docker.com/repository/docker/didias/devops-19-20-a-1191755
````
Para publicar as imagens "db" e "web" no Docker Hub

Fazer login no docker hub

`
$ docker login
`

Primeiro é ncessário fazer uma tag da imagem associada à conta Docker Hub.

````sh 

You need to include the namespace for Docker Hub to associate it with your account.
The namespace is the same as your Docker Hub account name.
You need to rename the image to YOUR_DOCKERHUB_NAME/docker-whale.
````

Para o container web:

`
$ docker tag ca4_web didias/devops19-20-a-1191755:web_spring_boot_application
`

de seguida fazer o push

`
$ docker push didias/devops19-20-a-1191755:web_spring_boot_application
`

Repetir o processo para o container db:


```` sh
$ docker tag ca4_db didias/devops19-20-a-1191755:db_spring_boot_application

$ docker push didias/devops19-20-a-1191755:db_spring_boot_application
````

Para fazer o pull dos containers realizar os seguinte comandos:

```` sh
$ docker pull didias/devops19-20-a-1191755:web_spring_boot_application

$ docker pull didias/devops19-20-a-1191755:db_spring_boot_application
````

###Alternativa

Como uma alternativa ao Docker foi analizado o Kubernetes. 
Uma vez colocados lado a lado não se pode dizer que sejam concorrentes diretos.
O Docker é uma plataforma de containers e o Kubernetes é um orquestrador de 
containers para plataformas como o Docker.

O Docker é uma ferramenta que serve para construir, distribuir e executar 
containers do Docker. Ele possui a sua própria ferramenta de cluster nativa 
que pode ser usada para orquestrar e agendar containers em clusters de máquinas. 
O Kubernetes é um sistema de orquestração de containers para containers do Docker 
Trabalha com o conceito de pods, que 
são unidades de agendamento (e podem conter um ou mais containers) no ecossistema 
Kubernetes e são distribuídos entre os nodes para fornecer alta disponibilidade. 
Pode-se executar facilmente uma compilação do Docker em um cluster Kubernetes, 
mas o próprio Kubernetes não é uma solução completa e deve incluir plug-ins 
personalizados.

Kubernetes e Docker são tecnologias fundamentalmente diferentes, mas funcionam 
muito bem em conjunto e facilitam o gerenciamento e a implantação de containers
numa arquitetura distribuída.