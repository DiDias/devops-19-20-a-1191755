package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {


    /**
     * First Name
     */

    @Test
    @DisplayName("Test set for first name - happy case")
    public void setForName(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        employee1.setFirstName("Manuel");
        String expected = "Manuel";
        //Assert
        assertEquals(expected,employee1.getFirstName());
    }
    @Test
    @DisplayName("Test set for first name - null case")
    public void setForNameNull(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setFirstName(null);
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("The first name can't be null!", ex.getMessage());
        }
    }

    @Test
    @DisplayName("Test set for first name - empty case")
    public void setForNameEmpty(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setFirstName("");
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("The first name can't be empty!", ex.getMessage());
        }
    }

    /**
     * Last Name
     */
    @Test
    @DisplayName("Test set for last name - happy case")
    public void setForLastName(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        employee1.setLastName("Pereira");
        String expected = "Pereira";
        //Assert
        assertEquals(expected,employee1.getLastName());
    }

    @Test
    @DisplayName("Test set for last name - null case")
    public void setForLastNameNull(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setLastName(null);
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("The last name can't be null!", ex.getMessage());
        }
    }
    @Test
    @DisplayName("Test set for last name - empty case")
    public void setForLastNameEmpty(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setLastName("");
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("The last name can't be empty!", ex.getMessage());
        }
    }

    /**
     * Description
     */
    @Test
    @DisplayName("Test set for description - happy case")
    public void setDescription(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        employee1.setDescription("extrovert");
        String expected = "extrovert";
        //Assert
        assertEquals(expected,employee1.getDescription());
    }

    @Test
    @DisplayName("Test set for description - null case")
    public void setForDescriptionNull(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setDescription(null);
        }
        //Assert
        catch (IllegalArgumentException ex) {
            assertEquals("The description can't be null!", ex.getMessage());

        }
    }

    @Test
    @DisplayName("Test set for last name - empty case")
    public void setForDescriptionEmpty(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setDescription("");
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("The description can't be empty!", ex.getMessage());
        }
    }

    /**
     * Job Title
     */
    @Test
    @DisplayName("Test set for job title - happy case")
    public void setJobTitle(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        employee1.setJobTitle("Baker");
        String expected = "Baker";
        //Assert
        assertEquals(expected,employee1.getJobTitle());
    }

    @Test
    @DisplayName("Test set for job title - null case")
    public void setForJobTitleNull(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setJobTitle(null);
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("The job title can't be null!", ex.getMessage());
        }
    }
    @Test
    @DisplayName("Test set for job title - empty case")
    public void setForJobTitleEmpty(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setJobTitle("");
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("The job title can't be empty!", ex.getMessage());
        }
    }

    /**
     * Email
     */
    @Test
    @DisplayName("Test set for email - happy case")
    public void setEmail(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        employee1.setEmail("jose@email.com");
        String expected = "jose@email.com";
        //Assert
        assertEquals(expected,employee1.getEmail());
    }

    @Test
    @DisplayName("Test set for email - null case")
    public void setForEmailNull(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setEmail(null);
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("Email can't be null! ", ex.getMessage());
        }
    }
    @Test
    @DisplayName("Test set for email - empty case")
    public void setForEmailEmpty(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setEmail("");
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("Email can't be empty! ", ex.getMessage());
        }
    }

    @Test
    @DisplayName("Test set for email - invalid")
    public void setForEmailInvalid(){
        //Arrange
        Employee employee1 = new Employee("José","Almeida","person","Woodworker","person@email.com");
        //Act
        try {
            employee1.setEmail("person");
        }
        //Assert
        catch (IllegalArgumentException ex){
            assertEquals("This email is not valide! ", ex.getMessage());
        }
    }
}