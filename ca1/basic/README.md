# Devops - Git Technical Report


Inicialmente foi criado um repositório remoto Bitbucket com o nome devops-19-20-a-1191755
e feita a ligação com a pasta IdeaProject.
```sh
$ cd IdeaProjects/
$ git remote add origin https://DiDias@bitbucket.org/DiDias/devops-19-20-a-1191755.git
$ git push -u origin --all
```
Automaticamente foi ser criado um repositório local devops-19-20-a-1191755 dentro da pasta IdeaProjects.

É então feito o download da pasta tutorial. 
Dentro do repositório local devops foi criada a pasta ca0 e colado o conteúdo da pasta tutorial. Todo o conteúdo foi mantido dentro do ca0 menos a pasta .git.

A pasta ca0 foi então adicionada ao repositório Bitbucket.
```sh
$ git init
$ git add .
$ git commit -m "Add files to repository"
```

A pasta ca0 foi aberta no IntelliJ e feitas as devidas alterações ao conteúdo (foi adicionada ao construtor de Employee uma String JobTitle). As alterações foram adicionadas ao Bitbucket.
```sh
$ git add .
$ git commit -m "Job Title field added"
```

Foi criada uma cópia de ca0, a ca1, utilizando a linha de comandos.
```sh
$ mkdir ca1
$ cp -r ca0/* ca1
$ git add .
$ git commit -m "Copy all files from ca0 to ca1"
$ git push -u origin --all
```

Para poder ter um track da última versão estável do trabalho, o último commit foi marcado com um tag.
```sh
$ git tag -a v1.2.0 -m "Version 1.2.0"
$ git push -u origin --tag
```

Foi depois adicionado um branch para corrigir bugs, fixed-invalid-email.
```sh
$ git checkout -b fix-invalid-email
```

Voltando depois ao branch email-field, foram adicionados testes para todos os gets e sets das variáveis.
```sh
$ git add. 
$ git commit -m "Add tests"
$ git push -u origin --all
```
Após a criação das condições e testes de validação foi feito um merge com o master.
```sh
$ git checkout master
$ git merge email-field
$ git add .
$ git push -u origin --all
```

Esta última adição foi marcada com o tag v1.3.1
```sh
$ git tag -a v1.3.1 -m "Version 1.3.1"
$ git push -u origin --tag
```

#Opção com Mercurial

Inicialmente foi criado um repositório remoto HelixTeamHub com o nome devops-19-20-a-1191755-2
e feita a ligação com a pasta IdeaProject.
Os comandos Mercurial são relativamente parecidos com os comandos Git.
Ao invés de .git utiliza-se agora o .hg.
```sh
$ cd IdeaProjects/
$ hg init
$ hg commit -m "Initial commit"
$ hg push https://dianasoutodiasgmailcom@helixteamhub.cloud/slippery-scissors-3964/projects/devops-19-20-a-1191755/repositories/mercurial/devops-19-20-a-1191755-2
```
Automaticamente foi ser criado um repositório local devops-19-20-a-1191755 dentro da pasta IdeaProjects.

É então feito o download da pasta tutorial. 
Dentro do repositório local devops foi criada a pasta ca0 e colado o conteúdo da pasta tutorial. Todo o conteúdo foi mantido dentro do ca0 menos a pasta .hg.

A pasta ca0 foi então adicionada ao repositório HelixTeamHub.
```sh
$ hg init
$ hg add 
$ hg commit -m "Add files to repository"
```

A pasta ca0 foi aberta no IntelliJ e feitas as devidas alterações ao conteúdo (foi adicionada ao construtor de Employee uma String JobTitle). As alterações foram adicionadas ao Bitbucket.
```sh
$ hg add 
$ hg commit -m "Job Title field added"
```

Foi criada uma cópia de ca0, a ca1, utilizando a linha de comandos.
```sh
$ mkdir ca1
$ cp -r ca0/* ca1
$ hg add 
$ hg commit -m "Copy all files from ca0 to ca1"
$ hg push https://dianasoutodiasgmailcom@helixteamhub.cloud/slippery-scissors-3964/projects/devops-19-20-a-1191755/repositories/mercurial/devops-19-20-a-1191755-2
```

Para poder ter um track da última versão estável do trabalho, o último commit foi marcado com um tag.
```sh
$ hg tag -a v1.2.0 -m "Version 1.2.0"
$ hg push -u origin --tag
```

Foi depois adicionado um branch para corrigir bugs, fixed-invalid-email.
```sh
$ hg checkout -b fix-invalid-email
```

Voltando depois ao branch email-field, foram adicionados testes para todos os gets e sets das variáveis.
```sh
$ hg add. 
$ hg commit -m "Add tests"
$ hg push https://dianasoutodiasgmailcom@helixteamhub.cloud/slippery-scissors-3964/projects/devops-19-20-a-1191755/repositories/mercurial/devops-19-20-a-1191755-2
```
Após a criação das condições e testes de validação foi feito um merge com o master.
```sh
$ hg checkout master
$ hg merge email-field
$ hg add 
$ hg push https://dianasoutodiasgmailcom@helixteamhub.cloud/slippery-scissors-3964/projects/devops-19-20-a-1191755/repositories/mercurial/devops-19-20-a-1191755-2
```

Esta última adição foi marcada com o tag v1.3.1
```sh
$ hg tag -a v1.3.1 -m "Version 1.3.1"
$ hg push -u origin --tag
```

