package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void setFirstNameSuccessCase() {
        //Arrange:
        Employee employee = new Employee("Marta", "Pinheiro", "student", "developer");
        String expected = "Marta";

        //Act:
        String result = employee.getFirstName();

        //Assert:
        assertEquals(expected, result);
    }

    @Test
    void setFirstNameNullCase() {
        //Arrange + Act
        try {
            new Employee(null, "Pinheiro", "student", "developer");
        }

        //Assert
        catch (IllegalArgumentException description) {
            assertEquals("first name can't be null or empty", description.getMessage());
        }
    }

    @Test
    void setFirstNameEmptyCase() {
        //Arrange + Act
        try {
            new Employee("", "Pinheiro", "student", "developer");
        }

        //Assert
        catch (IllegalArgumentException description) {
            assertEquals("first name can't be null or empty", description.getMessage());
        }
    }

    @Test
    void setLastNameSuccessCase() {
        //Arrange:
        Employee employee = new Employee("Marta", "Pinheiro", "student", "developer");
        String expected = "Pinheiro";

        //Act:
        String result = employee.getLastName();

        //Assert:
        assertEquals(expected, result);
    }

    @Test
    void setLastNameNullCase() {
        //Arrange + Act
        try {
            new Employee("Marta", null, "student", "developer");
        }

        //Assert
        catch (IllegalArgumentException description) {
            assertEquals("last name can't be null or empty", description.getMessage());
        }
    }

    @Test
    void setLastNameEmptyCase() {
        //Arrange + Act
        try {
            new Employee("Marta", "", "student", "developer");
        }

        //Assert
        catch (IllegalArgumentException description) {
            assertEquals("last name can't be null or empty", description.getMessage());
        }
    }

    @Test
    void setDescriptionSuccessCase() {
        //Arrange:
        Employee employee = new Employee("Marta", "Pinheiro", "student", "developer");
        String expected = "student";

        //Act:
        String result = employee.getDescription();

        //Assert:
        assertEquals(expected, result);
    }

    @Test
    void setDescriptionNullCase() {
        //Arrange + Act
        try {
            new Employee("Marta", "Pinheiro", null, "developer");
        }

        //Assert
        catch (IllegalArgumentException description) {
            assertEquals("description can't be null or empty", description.getMessage());
        }
    }

    @Test
    void setDescriptionEmptyCase() {
        //Arrange + Act
        try {
            new Employee("Marta", "Pinheiro", "", "developer");
        }

        //Assert
        catch (IllegalArgumentException description) {
            assertEquals("description can't be null or empty", description.getMessage());
        }
    }

    @Test
    void setJobTitleSuccessCase() {
        //Arrange:
        Employee employee = new Employee("Marta", "Pinheiro", "student", "developer");
        String expected = "developer";

        //Act:
        String result = employee.getJobTitle();

        //Assert:
        assertEquals(expected, result);
    }

    @Test
    void setJobTitleNullCase() {
        //Arrange + Act
        try {
            new Employee("Marta", "Pinheiro", "student", null);
        }

        //Assert
        catch (IllegalArgumentException description) {
            assertEquals("job title can't be null or empty", description.getMessage());
        }
    }

    @Test
    void setJobTitleEmptyCase() {
        //Arrange + Act
        try {
            new Employee("Marta", "Pinheiro", "student", "");
        }

        //Assert
        catch (IllegalArgumentException description) {
            assertEquals("job title can't be null or empty", description.getMessage());
        }
    }
}