Relatório CA2
===================

Este relatório serve como um apoio à compreensão do trabalho efectuado durante
a execução da Class Assignment 2: Build Tools with Gradle.

Assim como o Assignment também o relatório estará dividido em 2 partes, com
a observação final que pretende analizar as diferenças entre as três principais
 Build Tools do JAVA: Gradle,  Maven e Ant.

Build Tools
===
As Build Tools são programas que automatizam a criação de aplicações executáveis a partir do código-fonte.
Building incorpora a compilação, vinculação e packaging do código num formato utilizável ou executável.

Pré-requisitos
-------------

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 6.2.2
   

Part 1
-----

Criou-se uma pasta **CA2** no repositório local e dentro da mesma a pasta **part1**.
Em seguida foi feito commit com as alterações para o repositório BitBucket.

```sh

$git add .
$git commit -m "Add ca2 part1 to repository"
$git push -u origin --all

```

Procedeu-se ao build do gradle com o seguinte comando:

```sh

$gradlew build

```

Para correr um servidor foi então executado, na command line, o seguinte comando:

```sh

$ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp < server port >

```
Sendo que o server port utilizado foi o **59001**.

Para correr um cliente foi aberta então outra command line, mantendo sempre a do 
servidor aberta, e excutando-se então o comando:

```sh

$ gradlew runClient

```

Foram posteriormente criados outros clientes (cada um na sua command line)
 com o objetivo de ver a interação entre os mesmos e testar a função de chat.
 
 

Nova Task
---------

Foi adionada uma nova task para correr o servidor.

Localizou-se o build.gradle dentro da pasta **part1** no IDE e foi adicionada
a seguinte:

```sh

  task runServer (type: JavaExec, dependsOn: classes){
      group = "DevOps"
      description = "Launches a server that supports a chat on localhost:59001 "

      classpath = sourceSets.main.runtimeClasspath

      main = 'basic_demo.ChatServerApp'

      args '59001'
  }

```

A task foi então executada na linha de comandos para verificar se 
realmente estava a funcionar. Foi utilizado então o comando:

```sh

  $ gradlew runServer

```

Seguidamente foi feito commit e push para o repositório BitBucket.

Adicionar um test unitário
-----

Foi adicionada uma pasta de testes com o objetivo de testar a class 
App. Para que este podesse ser executado foi necessária a dependência
JUnit 4.2, que foi então adicionada.

```sh

package basic_demo;


import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {
    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
}

```
As dependências do JUnit 4.2 foram também adicionadas no build.gradle:

```sh

dependencies {
    // Use Apache Log4J for logging
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    testImplementation 'junit:junit:4.12'
}

```

E para o teste:
```sh

test {
    useJUnit()

    maxHeapSize = '1G'
}

```

Commit e push para o repositório BitBucket.

Task Copy
----
Adicionou-se uma nova task to tipo Copy para fazer backup da source
da aplicação.

Para este efeito foi utilizado o comando:

```sh

  mkdir backup

```

E adicionada ao build.gradle a task backupCopy:

```sh

task backupCopy(type: Copy) {
    from 'src'
    into 'backup'
}

```

Depois de verificar se a task estava a funcionar:

```sh

$ gradlew mkbackup

```
 
Foi feito commit e push para o repositório BitBucket.

Task Zip
-----

Adicionou-se uma nova task to tipo Zip para ser utilizado como arquivo
para da source da aplicação.

Esta foi então adicionada ao build.gradle:

```sh

task zip(type: Zip) {
    from 'src'
    archiveName 'project.zip'
}

```

Depois de verificar se a task estava a funcionar:

```sh

$ gradlew mkzip

```
Foi feito commit e push para o repositório BitBucket.

A part 1 depois de finalizada foi marcada com uma tag:

```sh

$ git tag -a ca2-part2 -m "ca2-part2"
$ git push -u origin --tag

```

Part 2
----

Para a execução da part2, inicialmente foi criado no repositório
BitBucket o branch **tut-basic-gradle** através do comando:

```sh

$ git checkout -b tut-basic-gradle

```

Posteriormente, na pasta CA2 foi criada a pasta **part2**.

Novo projeto Spring
----

Acedendo ao site Spring.io, foi criado um novo projeto Gradle com as
seguintes dependências:

* Rest Repositories;
* Thymeleaf;
* JPA;
* H2.

Foi então gerada uma pasta zip com o novo projeto Gradle em Java,
o Spring Boot e as dependências selecionadas.

O conteúdo da pasta zip foi então extraido para a pasta part2 de CA2.

E feito commit e push para o repositório BitBucket.

Conteúdo CA0
----

A pasta **src** foi eliminada e todos os conteúdos da pasta **basic** de CA0
 foram copiados para a part2.
 
 Para além da pasta **basic** foram também copiados:
 * webpack.config.js;
 * package.json.
 
Foi também eliminada a pasta build do static contida dentro de resources.
 
 
E feito commit e push para o repositório BitBucket.

Aplicação
-----

Para a aplicação poder ser experimentada foi necessário correr o 
comando:

```sh

$gradlew bootRun

```

Para que isso podesse acontecer o localhost:8080 tinha  estar livre.

Para o meu foi necessário forçar a paragem visto que já estava ocupado.

Foi então executado o seguinte comando: 

```sh

netstat  -ano  |  findstr  8080
taskkill /PID 656 /F

```

Adicionar plugin
---

Foi necessário agora adicionar o plugin org.siouan.frontend ao 
build.gradle:


```sh

frontend {
  nodeVersion = "12.13.1"
  assembleScript = "run webpack"
}

``` 

No package.json foi necessário adicionar à parte de scripts:

```sh

"scripts": {
  "watch": "webpack --watch -d",
  "webpack": "webpack"
},

```
E feito commit e push para o repositório BitBucket.

Front end gradle build
----

Para adicionar as tarefas relacionadas com front end foi necessário
fazer o build:

```sh

$gradlew build

```

Dist
---

Adicionou-se a pasta **dist** para copiar o ficheiro .jar

```sh

$mkdir dist

```

Foi depois criado no build.gradle a task copyJar:

```sh

task copyJar (type: Copy) {
	group = "DevOps"
	description = "Copy the generated jar to a folder named dist"
	from 'build/libs/'
	into 'dist'
}

```

Para verificar se a task criada funcionava, executou-se o comando:

```sh

$gradlew copyJar

```

Task deleteWebPack
---

Adicionou-se uma nova task que tem como objetivo apagar os ficheiros 
gerados por webpack.
 
Foi criada no build.gradle:
 
```sh

  task deleteWebpack (type: Delete) {
  	group = "DevOps"
  	description = "Deletes all the files generated by webpack"
  	delete 'src/main/resources/static/built'
  }

```

Para que cada vez que a task clean for executada, o dependsOn vai
fazer com que antes sejam apagados os ficheros webpack.
O comando utilizado para o efeito é:

```sh

clean.dependsOn deleteWebpack

```

Para testar que a task esteja a ser executada corretamente foi utilizado
o comando:

```sh

$gradlew clean

```
Feito depois commit e push para o repositório BitBucket.

Merge
----
Após concluir todas as tarefas da part2 foi necessário voltar para o 
master, fazemos então merge com o branch:

```sh

$git checkout master
$git merge tut-basic-gradle
$git add .
$git push -u origin –all

```

Para terminar, identificou-se o último commit realizado com a tag
ca2-part2:

```sh

$ git tag -a ca2-part2 -m "ca2 - part2"
$ git push -u origin --tag

```

ALTERNATIVA
=====
Ant, Maven e Gradle são Build Tools utilizadas para construir automaticamente processos para 
aplicações JAVA. 

Ant:

 * Os seus ficheiros são escritos em XML;
 * Controlo sobre todo o processo de build;
 * O seu principal beneficio é  flexibilidade:
     * Sem convenções;
     * Sem estrutura;
 * Ficheiros grandes e de dificil manutenção.

Maven:

 * Também utiliza ficheiros XML;
 * Possui comandos pré-definidos e convenções;
 * Suporte interno para gestão de dependencias;
 * Ponto mais forte é ciclo de vida;
 * Conflitos entre dependencias de versões diferentes;
 * Customização das convenções (goals) é dificil.


Gradle:

 * Mais recente, surge com o melhor dos dois:
	 * Flexibilidade->Ant;
	 * Estrutura e Convenções->Maven;
 * Os seus ficheros são escritos em DSL;
 * Scripts mais curtos e claros.

Em suma, existem várias Build Tools mas o Gradle é atualmente a que apresenta o 
melhor conjunto de caraterísticas: flixilidade, estrutura e clareza de apresentação.
É o mais indicado para projetos grandes e que necessitem constante manutenção.














