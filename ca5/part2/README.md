Class Assinment - ca5

##Jenkins

##PART1
Para realizar este assinment é necessária a criação de um diretório
ca5 part1 no repositório local.
Em seguida é necessário fazer download do jenkins.
Será transferido um ficheiro do tipo .war.


Seguidamente abrir uma command line ( no meu caso em específico, uma Bash),
e fazer o path até ao local onde se encontra o jenkins.war.

Correr então o comando 
````sh
java -jar jenkins.war
````

Uma vez em funcionamento deverá aparecer uma password para poder ser utilizada no
browser.

Uma vez aberto o localhost:8080 no browser, colocar a password no jenkins.

Seguidamente será necessário criar plugins, que neste caso ficarão os de default.

##Criar conta
Será necessário criar uma conta no Jenkins e dentro desta um novo pipeline.
É necessário colocar o path para o repositório desejado.

##Jenkinsfile
Em seguida, criar um Jenkinsfile com todos os dados para fazer o build do mesmo.

Dependendo do terminal utilizado, os comandos terão de ser adaptados. No meu caso,
como já referido utilizei comandos Bash.

Criar então os stages necessários para o build, dentro do Jenkinsfile.

É então criado um stage de Checkout, onde são locadas as credenciais git, 
para dar acesso ao repositório Bitbucket:

````sh

stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '1191755-devops', url: 'https://DiDias@bitbucket.org/DiDias/devops-19-20-a-1191755/'
    }

````

Em seguida, o Assemble para poder fazer o building do gradle, dentro do repositório
local desejado. Neste caso o ca2, part1:

````sh
stage('Assemble') {
            steps {
                dir('ca2/part1') {
                    echo 'Building...'
                    sh 'chmod +x gradlew'
                    sh './gradlew build -x test '
                }
            }
        }

````
Adicionar um stage para correr os testes:

````sh
stage('Test') {
            steps {
                dir('ca2/part1') {
                echo 'Testing...'
                sh 'chmod +x gradlew'
                sh './gradlew test' 
                sh 'touch build/test-results/test/*.xml'
                junit 'build/test-results/test/*.xml'
            }
            }
        }
````
E finalmente, o stage para o Archiving, para este ser guardado:

````sh
stage('Archiving') {
            steps {
                dir('ca2/part1') {
                sh 'pwd'
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/'
            }
            }
        }
    }
````
Deve ser então feito o push do Jenkinsfile para o repositório Bitbucket.
Seguidamente, ir ao Jenkins e fazer Build Now. Se tudo estiver correto, o build será
feito com sucesso.

####PART2

Para realizar a segunda parte do assinment, em primeiro lugar é necessário ser criado
um diretório part2 dentro do ca5.

Em seguida fazer clone do Jenkinsfile contido na part1.
Aqui o procedimento é o mesmo, send necessário correr o jenkins.war no terminal.


##Instalar o HTML publisher

Para a realização desta segunda parte, algumas alterações terão de ser feitas ao 
Jenkinsfile clonado.

Terá de ser instalado um plugin do jenkins correspodente ao HTML, o HTML publisher.

Em seguida, dentro o Jenkinsfile, acionar o stage correspondente:

````sh
stage ('Javadoc'){
            steps {
                dir('ca2/part2') {
                echo 'Generating javadocs...'
                sh 'chmod +x gradlew'
                sh './gradlew javadoc'
                publishHTML ([
                            reportName: 'Javadoc',
                            reportDir: 'build/docs/javadoc/',
                            reportFiles: 'index.html',
                            keepAll: false,
                            alwaysLinkToLastBuild: false,
                            allowMissing: false
                            ])
              }
            }
        } 
````

## DockerHub
Será necessária a criação de um novo repositório dentro do DockerHub.
Este foi chamado de ca5. 

Em seguida, correr o Docker Toolbox no terminal.

##Docker settings

Dentro do Jenkinsfile terão de ser criados 2 novos stages correspondentes ao build
de uma image no DuckerHub e ao publish da mesma. 
Assim sendo, para o build:

````sh
stage('Docker Image') {
             steps {
             dir('ca5/part2') {
                    script {
                    dockerimage = docker.build("didias/devops-19-20-a-1191755:${env.BUILD_ID}")
                    }
                }
            }
        }
````
E para o publish:

````sh
stage('Publish Image') {
            steps {
                dir('ca5/part2') {
                    script{
                        sh 'docker login -u="didias" -p="UDDsauFyJ9"'
                        dockerimage.push()
                    }
                }
            }
````

De novo, para poder fazer build desta para no jenkins, fazer push para o repositório 
Bitbucket e fazer Build Now.

Para esta segunda parte tive a necessidade fazer algumas alterações no stage Archiving.

````sh
stage('Archiving') {
            steps {
                dir('ca2/part2') {
                sh 'pwd'
                echo 'Archiving...'
                archiveArtifacts 'build/libs/'
            }
        }
}
````
Se tudo estive bem feito, dentro do repositório de DockerHub deverá aparecer uma imagem, 
que neste caso ficou correspondente ao número de tentativa de build no jenkins, a imagem
14.

### Alternativa

A alternativa utilizada para este assinment foi o Buddy.
Assim sendo, é necessária a criação de uma conta no Buddy, dar permissão de acesso
ao repositório Bitbucket e criar actions, correspondestes aos stages no Jenkinsfile.
As actions criadas são então similares, para fazer as mesmas tarefas, no caso do Buddy
a linguagem é bastante mais simples.

Para o Assemble, na parte de run:
````sh
cd ca2/part2
rm -r node_modules
npm install
chmod +x gradlew
./gradlew build -x test
echo "Building..."
gradle clean assemble
````
e em environment, ainda no Assemble:
````sh
apt-get update
apt-get install -f
apt-get -y install git
apt-get install nodejs -y
apt-get install npm -y
````
Para os testes:
````sh
cd ca2/part2   
echo 'Testing...'
gradle test
````
Para execução dos Javadocs:
````sh
cd ca2/part2
echo 'Generating javadocs...'
gradle javadoc
````

No que toca ao Docker, é necessário mostrar o caminho para o diretório
onde se encontra o Dockerfile.

````sh
ca5/part2/Dockerfile
````
Para o push da imagem são necessários o user e a password do DockerHub, bem 
como do nome do repositório dentro do mesmo:
````sh
didias/devops-19-20-a-1191755
````
O push pode ser também marcado com uma tag. O próprio Buddy sugere algumas:

````sh
$BUDDY_EXECUTION_ID
````
Para concluir, é só necessário correr o Run pipeline. 
O Buddy irá perguntar para qual commit do repositório deve ser feito, e é necessário
apenas fazer o Run Now.

Dentro do DockerHub deve aparecer uma nova imagem correspondente ao push dado
e no Bitbucket a confirmação de que o commit foi feito com sucesso.


##Conclusão

O Jenkins é então a ferramenta open source de automação mais utilizada por devolopers
de Java para integração continua. Torna o build e o testing dos projetos mais fácil,
permitindo manter um track ao longo do projeto.

O Buddy é então uma ferramenta que se encaixa no perfil de desenvolvimento continuo,
é não de integração continua, como é o caso do Jenkins. É utilizado na sua maioria
para projetos que envolvam Docker.
Ao contrário do Jenkins, o Buddy não precisa de instalação prévia e um dos fatores
que realmente realçaram durante o decorrer deste assinment foi o facto de ser
mais user friendly. Não há a necessidade de criar files com os stages, visto que
o próprio Buddy apresenta de uma maneira muito simples e clean os passos a ser seguidos.
O código utilizado para a realização das actions também é bastante mais simplificado.

Em suma, ambas as plataformas cumpriram o necessário para a realização de trabalhos
simples como este, sendo que o Buddy sai a ganhar no quesito de ser mais user friendly
e visually appealing.



